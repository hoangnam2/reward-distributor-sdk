# Reward Distributor SDK
This repository helps generate signatures for Reward Distributor services.

## Example Usage

```
        go get gitlab.com/hoangnam2/reward-distributor-sdk@latest


        requestID, err := rdsdk.GenerateRandomRequestID()
	if err != nil {
		panic(err)
	}
	
	or requestID := rdsdk.GenerateRequestID([]byte("Hello|1.89|930000))
	
	request := rdsdk.GenerateSignatureRequest{
		TenantID:  "0x69560b483eF668097d3bDc4C4b4f33F442d4Cf88",
		ChainID:   930000,
		RequestID: requestID,
		Asset:     "0x9AC215Dcbd4447cE0aa830Ed17f3d99997a10F5F",
		Amount:    decimal.NewFromFloat(1.991),
		Recipient: "0x9AC215Dcbd4447cE0aa830Ed17f3d99997a10F5F",
	}
	signature := rdsdk.GenerateSignature("811d0c90560f9c8be6f7f9862ef6369263f3fffdbd07d2ecee0129a3ce72b74d", request)
```
