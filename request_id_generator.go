package rdsdk

import (
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/google/uuid"
)

// GenerateRequestID returns the Keccak256 hash of the input data
func GenerateRequestID(data []byte) string {
	return generateRequestID(data)
}

// GenerateRandomRequestID returns the Keccak256 hash from a random UUID
func GenerateRandomRequestID() (string, error) {
	randomUUID, err := uuid.NewRandom()
	if err != nil {
		return "", err
	}
	return generateRequestID([]byte(randomUUID.String())), nil
}

func generateRequestID(data []byte) string {
	return crypto.Keccak256Hash(data).Hex()
}
