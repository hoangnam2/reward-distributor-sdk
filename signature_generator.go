package rdsdk

import (
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/shopspring/decimal"
)

// GenerateSignatureRequest represents the request for generating signature.
// TenantID is the ID of tenant in Reward Distributor database and act like a public key for verifying generated signature.
// ChainID is the destination chain for making transactions.
// RequestID is the ID of the request. This ID is to prevent replaying duplicated transactions.
// Asset is the ERC20 token address to distribute, use 0x0000000000000000000000000000000000000001 for native coin.
// Amount is the amount to distribute.
// Recipient is the distribution recipient address.
// Refer: https://technixocom.atlassian.net/wiki/spaces/TECHNIXO/pages/407208027/Reward+Distributor+-+Kafka+Message+Specifications
type GenerateSignatureRequest struct {
	TenantID  string
	ChainID   int64
	RequestID string
	Asset     string
	Amount    decimal.Decimal
	Recipient string
}

// GenerateSignature returns signature generated from GenerateSignatureRequest.
// privateKeyStr is the private key in string format. This key must match with TenantID
func GenerateSignature(privateKeyStr string, req GenerateSignatureRequest) (string, error) {
	privateKeyECDSA, err := crypto.HexToECDSA(privateKeyStr)
	if err != nil {
		return "", err
	}
	hash := GeneratePayloadHash(req)
	sig, err := crypto.Sign(hash.Bytes(), privateKeyECDSA)
	if err != nil {
		return "", err
	}
	return hexutil.Encode(sig), nil
}

// GeneratePayloadHash returns a payload has in Keccak256 format.
// This payload hash will be used to sign the message.
func GeneratePayloadHash(req GenerateSignatureRequest) common.Hash {
	return crypto.Keccak256Hash([]byte(fmt.Sprintf("%s|%d|%s|%s|%s|%s",
		req.TenantID,
		req.ChainID,
		req.RequestID,
		req.Asset,
		req.Amount,
		req.Recipient,
	)))
}
