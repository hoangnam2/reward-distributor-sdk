module gitlab.com/hoangnam2/reward-distributor-sdk

go 1.19

require (
	github.com/ethereum/go-ethereum v1.10.16
	github.com/google/uuid v1.2.0
	github.com/shopspring/decimal v1.3.1
)

require (
	github.com/btcsuite/btcd v0.20.1-beta // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
)
