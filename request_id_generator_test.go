package rdsdk

import (
	"testing"
)

func TestGenerateRequestID(t *testing.T) {
	type args struct {
		data []byte
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test generate request id",
			args: args{
				data: []byte("Hello there|123|1.23|6.9999999|123123123|asdasdzczxcasdqwer|asdasdzczxcasdqwer"),
			},
			want: "0x6e240d6dbbec802808fb23bebfb22bfc7dd713c83010df50dc0162aa395f30dc",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GenerateRequestID(tt.args.data); got != tt.want || len(got) != 66 {
				t.Errorf("GenerateRequestID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGenerateRandomRequestID(t *testing.T) {
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			name:    "test random request id generator, should not throw error",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GenerateRandomRequestID()
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateRandomRequestID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) != 66 {
				t.Errorf("GenerateRandomRequestID() got = %v, invalid length", got)
			}
		})
	}
}
