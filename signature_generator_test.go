package rdsdk

import (
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/shopspring/decimal"
	"testing"
)

func TestGenerateSignature(t *testing.T) {
	type args struct {
		privateKeyStr string
		req           GenerateSignatureRequest
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "test generate signature with valid request",
			args: args{
				privateKeyStr: "811d0c90560f9c8be6f7f9862ef6369263f3fffdbd07d2ecee0129a3ce72b74d",
				req: GenerateSignatureRequest{
					TenantID:  "0x69560b483eF668097d3bDc4C4b4f33F442d4Cf88",
					ChainID:   930000,
					RequestID: GenerateRequestID([]byte("1.89|hello")),
					Asset:     "0x9AC215Dcbd4447cE0aa830Ed17f3d99997a10F5F",
					Amount:    decimal.NewFromFloat(1.991),
					Recipient: "0x9AC215Dcbd4447cE0aa830Ed17f3d99997a10F5F",
				},
			},
			wantErr: false,
		},
		{
			name: "test generate signature with invalid public key",
			args: args{
				privateKeyStr: "811d0c90560f9c8be6f7f9862ef6369263f3fffdbd07d2ecee0129a3ce72b74d",
				req: GenerateSignatureRequest{
					TenantID:  "0x9AC215Dcbd4447cE0aa830Ed17f3d99997a10F5F",
					ChainID:   930000,
					RequestID: GenerateRequestID([]byte("1.89|hello")),
					Asset:     "0x69560b483eF668097d3bDc4C4b4f33F442d4Cf88",
					Amount:    decimal.NewFromFloat(1.991),
					Recipient: "0x9AC215Dcbd4447cE0aa830Ed17f3d99997a10F5F",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			signature, err := GenerateSignature(tt.args.privateKeyStr, tt.args.req)
			if err != nil {
				t.Errorf("GenerateSignature() error = %v", err)
				return
			}
			if err := verifySignature(tt.args.req, signature); (err != nil) != tt.wantErr {
				t.Errorf("GenerateSignature() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func verifySignature(req GenerateSignatureRequest, signature string) error {

	publicKey := common.HexToAddress(req.TenantID)
	payloadHash := GeneratePayloadHash(req)

	signatureBytes, err := hexutil.Decode(signature)
	if err != nil {
		return fmt.Errorf("verify signature error: invalid signature. %v", err)
	}

	publicKeyFromSignature, err := crypto.SigToPub(payloadHash.Bytes(), signatureBytes)
	if err != nil {
		return fmt.Errorf("verify signature error: recover public key from payload failed. %v", err)
	}

	if publicKey != crypto.PubkeyToAddress(*publicKeyFromSignature) {
		return fmt.Errorf("verify signature error: public key recover from payload not match with provided public key")
	}
	return nil
}
